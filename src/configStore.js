import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware, { END } from 'redux-saga';
import { persistStore, autoRehydrate } from 'redux-persist';
import { REHYDRATE, PURGE, persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/lib/storage' // or whatever storage you are using

import logger from 'redux-logger';

import rootReducer from './reducers';
import rootSaga from './sagas';
import { STORAGE_CONFIG } from './constants/config';

export default function configStore(initialState = {}) {
    const sagaMiddleware = createSagaMiddleware();
    const store = createStore(
        rootReducer,
        initialState,
        compose(
            applyMiddleware(
                sagaMiddleware,
                logger,
            )
        )
    );
    sagaMiddleware.run(rootSaga);

    if (module.hot) {
        module.hot.accept(() => {
            const nextRootReducer = require('./reducers').default;
            store.replaceReducer(nextRootReducer);
        });
    }

    return store;
}

