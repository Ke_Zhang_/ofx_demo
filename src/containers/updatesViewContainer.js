import React from 'react';
import { connect } from 'react-redux';
import { connectEnhance } from '../screens';
import { bindActionCreators } from 'redux';

import * as viewActions from '../actions/updatesViewActions.js';

import UpdatesView from '../components/updatesView';
import SafeAreaView from 'react-native-safe-area-view';

const updatesViewContainer = ({ ...props }) => (
    <UpdatesView {...props} />
);

function mapStateToProps(state) {
  return {
    newsData: state.updatesReducer.newsData
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...viewActions }, dispatch);
}


export default connectEnhance(updatesViewContainer, mapStateToProps, mapDispatchToProps);