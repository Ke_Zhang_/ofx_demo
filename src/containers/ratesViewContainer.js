import React from 'react';
import { connect } from 'react-redux';
import { connectEnhance } from '../screens';
import { bindActionCreators } from 'redux';

import * as viewActions from '../actions/ratesViewActions.js';

import RatesView from '../components/ratesView';
import SafeAreaView from 'react-native-safe-area-view';

const ratesViewContainer = ({ ...props }) => (
    <RatesView {...props} />  
);

function mapStateToProps(state) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...viewActions}, dispatch);
}

export default connectEnhance(ratesViewContainer, mapStateToProps, mapDispatchToProps);