/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import { Platform } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
import configStore from './configStore';
import { registerScreens } from './screens';
import { ratesViewContainer, updatesViewContainer } from './containers';

export default class App {
  static startApp() {

    registerScreens(configStore(), Provider);

    Navigation.events().registerAppLaunchedListener(() => {
      Navigation.setRoot({
        root: {
          bottomTabs: {
            children: [{
              stack: {
                children: [{
                  component: {
                    name: ratesViewContainer.displayName,
                    passProps: {
                      text: 'some props may need to pass'
                    }
                  }
                }],
                options: {
                  bottomTab: {
                    text: 'RATES',
                    icon: require('./common/images/Rates.png'),
                    selectedIcon: require('./common/images/Rates-Active.png'),
                    testID: ratesViewContainer.displayName
                  }
                }
              }
            },
            {
              component: {
                name: updatesViewContainer.displayName,
                passProps: {
                  text: 'some props may need to pass'
                },
                options: {
                  bottomTab: {
                    text: 'UPDATES',
                    icon: require('./common/images/Updates.png'),
                    selectedIcon: require('./common/images/Updates-Active.png'),
                    testID: ratesViewContainer.displayName
                  }
                }
              }
            },
            {
              component: {
                name: updatesViewContainer.displayName,
                passProps: {
                  text: 'some props may need to pass'
                },
                options: {
                  bottomTab: {
                    text: 'TRANSFERS',
                    icon: require('./common/images/Transfers.png'),
                    selectedIcon: require('./common/images/Transfers-Active.png'),
                    testID: ratesViewContainer.displayName
                  }
                }
              }
            },
            {
              component: {
                name: updatesViewContainer.displayName,
                passProps: {
                  text: 'some props may need to pass'
                },
                options: {
                  bottomTab: {
                    text: 'RECIPIENTS',
                    icon: require('./common/images/Recipients.png'),
                    selectedIcon: require('./common/images/Recipients-Active.png'),
                    testID: ratesViewContainer.displayName
                  }
                }
              }
            },
            {
              component: {
                name: updatesViewContainer.displayName,
                passProps: {
                  text: 'some props may need to pass'
                },
                options: {
                  bottomTab: {
                    text: 'SETTINGS',
                    icon: require('./common/images/Settings.png'),
                    selectedIcon: require('./common/images/Settings-Active.png'),
                    testID: ratesViewContainer.displayName
                  }
                }
              }
            }
            ]
          }
        }
      });
    });
  }
}
