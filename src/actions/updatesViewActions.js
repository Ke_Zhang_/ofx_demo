import {
    LOAD_MORE,
    LOAD_MORE_RESPONSE
} from '../constants/actionTypes';

export function loadMoreRequest(request) {
  return {
    type: LOAD_MORE,
    request
  };
}

export function loadMoreResponse(response) {
  return {
    type: LOAD_MORE_RESPONSE,
    response
  };
}