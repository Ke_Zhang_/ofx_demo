import {
    DO_SOMETHING,
    DO_SOMETHING_RESPONSE
} from '../constants/actionTypes';

export function doSomethingRequest(request) {
  return {
    type: DO_SOMETHING,
    request
  };
}

export function doSomethingResponse(response){
    return {
        type: DO_SOMETHING_RESPONSE,
        response
    };
}