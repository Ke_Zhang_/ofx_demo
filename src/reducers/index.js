import {combineReducers} from 'redux';

import ratesReducer from './ratesReducer';
import updatesReducer from './updatesReducer';


export default combineReducers({
    ratesReducer,
    updatesReducer
});