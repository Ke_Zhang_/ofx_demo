import { find } from 'lodash';
import * as actionTypes from '../constants/actionTypes';


export default function updatesReducer(state = {
  newsData: []
}, action) {
  switch (action.type) {
    case actionTypes.LOAD_MORE_RESPONSE:
      return {
        newsData: action.data
      }
    default:
      return state;
  }
}
