import { connect } from 'react-redux';
import { compose } from 'redux';
import { Navigation } from 'react-native-navigation';
import withSafeAreaView from './components/hocs/withSafeAreaView';

let screens = [];

export function connectEnhance(Component, mapStateToProps = function () { return {} }, mapDispatchToProps = function () { return {} }, mergeProps, options) {
  const connected = connect(mapStateToProps, mapDispatchToProps, mergeProps, options);
  const enhance = compose(
    connected
  )
  let Screen = enhance(Component);
  Screen.displayName = Component.name+screens.length; // 
  screens.push(Screen);
  return Screen;
}

export function registerScreens(store, Provider) {
  screens.forEach((Screen) => {
    Navigation.registerComponentWithRedux(
      Screen.displayName,
      () => Screen,
      //() => withSafeAreaView(Screen), // wrap all components with safe area on iPhoneX,XR and X MAX
      Provider,
      store
    );
  });
}