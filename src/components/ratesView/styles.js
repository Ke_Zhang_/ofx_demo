import { StyleSheet } from 'react-native';
import CommonStyle from '../../common/commonStyles';

// const { SCREEN, THEME_COLOR, fonts } = COMMON_STYLE;
// const threeImgWidth = (SCREEN.width - 52) / 3;
// const twoImgWidth = (SCREEN.width - 40) / 2;
const Styles = StyleSheet.create({
    title: {
        ...CommonStyle.fonts.M2,
    }
});

export default Styles;
