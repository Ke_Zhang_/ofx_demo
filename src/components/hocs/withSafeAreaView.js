import React, { Component } from 'react';
import SafeAreaView from 'react-native-safe-area-view';

const withSafeAreaView = (WrappedComponent) => {

  class SafeAreaViewEnhanced extends React.PureComponent {
    render() {
      return (<SafeAreaView><WrappedComponent {...this.props} /></SafeAreaView>);
    }
  }

  SafeAreaViewEnhanced.displayName = `SafeAreaViewEnhanced(${WrappedComponent.displayName || WrappedComponent.name})`
  return SafeAreaViewEnhanced;
}

export default withSafeAreaView;