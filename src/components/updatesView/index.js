import React, {
  PureComponent
} from 'react';

import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StatusBar,
  ScrollView,
  FlatList
} from 'react-native';

import Styles from './styles';

import ContentCell from './contentCell';

export default class UpdatesView extends PureComponent {

  constructor(props) {
    super(props);
    // console.log('123');
  }

  componentDidMount(){
    this.props.loadMoreRequest();
  }
  // componentWillReceiveProps(NextProps) {
  //   console.log('componentWillReceiveProps', NextProps);
  // }

  onClick = (e) => {
    console.log(e);
  }
  render() {
    const { newsData } = this.props;

    return (
      <View>
        <StatusBar barStyle={'light-content'} />
        {/*Poster begin*/}
        <View style={Styles.container}>
          <Image
            source={require('../../common/images/newyork.jpg')}
            style={Styles.imageOverlay}
          />
          <View style={Styles.titleLayout}>
            <View style={Styles.titleLeft}>
              <Text style={Styles.title}>UPDATES
                      </Text>
            </View>
            <TouchableOpacity style={Styles.titleRight}>
              <Text style={Styles.subTitle}>Manage</Text>
            </TouchableOpacity>
          </View>
          <View style={{ padding: 20 }}>
            <View>
              <Text style={Styles.titleDate}>
                08:56AM Today
                        </Text>
            </View>
            <View>
              <Text style={Styles.titleLarge}>
                Greenback firms as the Fed moves closer to rate a hike
                        </Text>
            </View>
          </View>
        </View>
        {/*Poster end*/}
        
        <FlatList 
          style={Styles.flatList}
          data={newsData}
          renderItem={({item}) => <ContentCell news={item} action={this.onClick} />}
          //keyExtractor={item => item.key} // NOTE: if the data from real database each item should have an unique key
        />
      </View>
    );
  }
}
