import { StyleSheet } from 'react-native';
import CommonStyle from '../../common/commonStyles';

// const { SCREEN, THEME_COLOR, fonts } = COMMON_STYLE;
// const threeImgWidth = (SCREEN.width - 52) / 3;
// const twoImgWidth = (SCREEN.width - 40) / 2;
const Styles = StyleSheet.create({
    container: {
        height:CommonStyle.screen.width/1.6
    },
    imageOverlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: CommonStyle.screen.width,
        height: CommonStyle.screen.width/1.6,
        backgroundColor: 'black',
        opacity: 1.2
    },
    titleLayout: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center',
        width: CommonStyle.screen.width,
        marginTop:CommonStyle.screen.height * 0.05
    },
    titleLeft: {
        flexDirection:'row',
        justifyContent:'flex-end', 
        width:CommonStyle.screen.width/1.65
    },
    titleRight: {
        flexDirection:'row',
        justifyContent:'flex-end', 
        width:CommonStyle.screen.width/3
    },
    title: {
        ...CommonStyle.fonts.XL,
        color: CommonStyle.themeColor.white
    },
    subTitle: {
        ...CommonStyle.fonts.M,
        color: CommonStyle.themeColor.white
    },
    titleDate: {
        ...CommonStyle.fonts.M,
        color: CommonStyle.themeColor.graybackground,
    },
    titleLarge:{
        ...CommonStyle.fonts.XXL,
        color: CommonStyle.themeColor.white,
        lineHeight:25,
        paddingTop: 35 - (35 * 0.75),
    },
    cellContainer: {
        height: 80, 
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        padding:20
    },
    cellLeft: {
        width:CommonStyle.screen.width/4.5,
        height:75,
        justifyContent:'center',
        alignItems:'center'
    },
    cellRight: {
        width:CommonStyle.screen.width * 0.8,
        height:75,
        justifyContent:'flex-start',
        alignItems:'flex-start',
        paddingVertical:14,
        paddingRight:30
    },
    cellDateText: {
        ...CommonStyle.fonts.S,
        color: CommonStyle.themeColor.greyishText
    },
    cellTitleText: {
        ...CommonStyle.fonts.M2,
        color: CommonStyle.themeColor.black,
        lineHeight:17
    },
    flatList: {
        marginBottom: CommonStyle.screen.height * 0.3,
    }

});

export default Styles;