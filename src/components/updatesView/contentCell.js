import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    TouchableHighlight
} from 'react-native';
import Styles from './styles';

const ContentCell = ({ news,action }) => {

    return (
        <TouchableHighlight onPress={()=>action('something is pressed!')} underlayColor='#edeeed'>
            <View style={Styles.cellContainer}>
                <View style={Styles.cellLeft}>
                    <View>
                        <Image
                            source={{ uri: news.image }}
                            style={{ height: 50, width: 50, borderRadius: 25 }}
                        />
                    </View>
                </View>
                <View style={Styles.cellRight}>
                    <Text style={Styles.cellDateText}>
                        {news.date}
                    </Text>
                    <Text numberOfLines={2} style={Styles.cellTitleText}>
                        {news.title}
                    </Text>
                </View>
            </View>
        </TouchableHighlight>
    )
};

export default ContentCell