import {delay} from 'redux-saga';
import {
    fork,
    cancel,
    call,
    put,
    race,
    take,
    takeLatest,
    END
} from 'redux-saga/effects';
import * as actionTypes from '../constants/actionTypes';
import * as ratesViewActions from '../actions/ratesViewActions';
import {RUN_TIME_SETTINGS} from '../constants/config';

function* doSomethingRequest() {
    while (true){
        const doSomethingRequest = yield take(actionTypes.DO_SOMETHING);
        const response = yield call(doSomethingResponse, ratesViewActions.doSomethingRequest);
        const responseAction = ratesViewActions.doSomethingResponse(response);
        yield put(responseAction);
    }
}

function doSomethingResponse(demoResponse) { // this is just a mock response function
    return {
        something:'test'
    };
}

export default function * ratesViewSaga() {
    yield[
        fork(doSomethingRequest),
    ];
}