import { fork } from 'redux-saga/effects';
import ratesViewSaga from './ratesViewSaga';
import updatesViewSaga from './updatesViewSaga';

export default function* rootSaga() {
  yield [
    fork(ratesViewSaga),
    fork(updatesViewSaga),
  ];
}