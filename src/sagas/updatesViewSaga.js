import { delay } from 'redux-saga';
import {
    fork,
    cancel,
    call,
    put,
    race,
    take,
    takeLatest,
    END
} from 'redux-saga/effects';
//import data from './data/news.json';
import axios from "axios";
import * as actionTypes from '../constants/actionTypes';
import * as updatesViewActions from '../actions/updatesViewActions';


function* loadMoreRequest() {
    while (true) {
        const loadMoreRequest = yield take(actionTypes.LOAD_MORE);
        const data = yield call(fetchNews);
        yield put({ type: actionTypes.LOAD_MORE_RESPONSE, data:data.data});
    }
}

function fetchNews() { // this is a real api call
    return axios.get("https://966.com.au/news.json"); //TODO: Store the base URL somewhere else
}

export default function* updatesViewSaga() {
    yield [
        fork(loadMoreRequest),
    ];
}