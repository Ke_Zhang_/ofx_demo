import { AsyncStorage } from 'react-native';


export const STORAGE_CONFIG = {
  storage: AsyncStorage,
  whiteList: [
    'ratesReducer',
    'updatesReducer'
  ],
  blacklist: [
    
  ],
};

export const SERVER_URL = '';

const ENVIRONMENTS = {
  offline: 'offline',
  dev: 'dev',
  test: 'test',
  prod: 'prod',
}

function getRunTimeSettings() {
  const env = ENVIRONMENTS.offline;

  switch (env) {
    case ENVIRONMENTS.dev:
      return {
        webBaseUri: '',
        mockCallsToServer: false
      }
    case ENVIRONMENTS.test:
      return {
        webBaseUri: '',
        mockCallsToServer: false
      }
    case ENVIRONMENTS.prod:
      return {
        webBaseUri: '',
        mockCallsToServer: false
      }
    case ENVIRONMENTS.offline: // Might be redundant if offline is the chosen state of default settings
    default:
      return {
        webBaseUri: null,
        mockCallsToServer: true
      }
  }
}

export const RUN_TIME_SETTINGS = getRunTimeSettings();
