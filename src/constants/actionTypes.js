export const LOAD_MORE = 'LOAD_MORE';
export const LOAD_MORE_RESPONSE = 'LOAD_MORE_RESPONSE';
export const DO_SOMETHING = 'DO_SOMETHING';
export const DO_SOMETHING_RESPONSE = 'DO_SOMETHING_RESPONSE';